class Module141
{
  int column1;
  Module141 (int operand1)
  { // this is NOT implicit operand 
    this.column1 = operand1;
  } // this is implicit local-object
  
  void operation1 (/* Module141 this */)
  { // write pseudo-code of operation1 
    Console.WriteLine (this.column1);
  } // to understand how it works
  
  public static void main (String[] args)
  {
    Module141 RecordObject1 = new Module141(1);
    Module141 RecordObject2 = new Module141(2); 
    RecordObject1.operation1(); // operation1 (RecordObject1) 
    RecordObject2.operation1(); // operation1 (RecordObject2)
  }
}