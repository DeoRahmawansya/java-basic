class Basic4
{ // no error, implicit value-conversion
  public static void main (String[] args)
  {
    Boolean object1 = true;
    Byte object2 = 1;
    Character object3 = 'c';
    Double object4 = 1.5;
    Float object5 = -0.5f;
    Integer object6 = 6;
    Long object7 = 7L;
    Short object8 = (short)8;
    boolean object91 = object1;
    byte object92 = object2;
    char object93 = object3;
    double object94 = object4;
    float object95 = object5;
    int object96 = object6;
    long object97 = object7;
    short object98 = object8;
  }
}