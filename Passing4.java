class Passing4
{
  private static NotEmpty temp = new NotEmpty(' ');

  public static void swap (NotEmpty operand1, NotEmpty operand2)
  {
    temp.column1 = operand1.column1;
    operand1.column1 = operand2.column1;
    operand2.column1 = temp.column1;
  }
  
  public static void main (String[] args)
  {
    NotEmpty Object1 = new NotEmpty('a');
    NotEmpty Object2 = new NotEmpty('b');
    System.out.println 
      ("Values before swapping: " + Object1.column1 + ", " + Object2.column1);
    swap (Object1, Object2);
    System.out.println 
      ("Values after swapping: " + Object1.column1 + ", " + Object2.column1); 
  }
}