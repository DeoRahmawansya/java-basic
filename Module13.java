class Module13
{
  /* dynamic */ int column1;
  
  Module13 (int operand1)
  { 
    this.column1 = operand1;
  }

  public static void main (String[] args)
  { // you canNOT statically allocate ANY record-object
    Module13 RecordObject1 = Module13 (1);
    int object1 = 1;
  } // you can statically allocate any basic-object
}