class Module10
{
  /* dynamic */ int column1;
  
  Module10 Module10 (int operand1)
  { // this operation is considered valid
    this.column1 = operand1;
    return (this);
  } // but calling it is considered invalid

  public static void main (String[] args)
  { // try calling operation Module10
    Module10 RecordObject1 = new Module10 (1);
  } // without actual-operand
}