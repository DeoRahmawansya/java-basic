public class Passing6
{
  int column1;
  
  Passing6 (int initial_value)
  {
    column1 = initial_value;
  }
  
  void operation1 (/*Passing6 this*/)
  { // the operand is now implicit
    this.column1 += 1;
  } // the name MUST be system-defined
  
  public static void main (String[] args)
  {
    Passing6 Object1 = new Passing6 (1);
    System.out.println (Object1.column1);    
    Object1.operation1 (); // implicit operand
    System.out.println (Object1.column1); 
  }
}