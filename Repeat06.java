public class Repeat06
{ // about repetition-operation; do while
  public static void main (String[] args)
  {
    int[] Object1 = new int[] {9, 8, 7, 6, 5};
    int counter = 0, count = Object1.length;
    do
    {
      System.out.print (Object1[counter]);
      counter++;
    }
    while (counter < count);
    System.out.println ();
  }
}