class Passing1
{
  public static void swap (Character operand1, Character operand2)
  {
    Character temp = operand1;
    operand1 = operand2;
    operand2 = temp;
  }
  
  public static void main (String[] args)
  {
    Character object1 = 'a';
    Character object2 = 'b';
    System.out.println ("Values before swapping: " + object1 + ", " + object2);
    swap (object1, object2);
    System.out.println ("Values after swapping: " + object1 + ", " + object2); 
  }
}