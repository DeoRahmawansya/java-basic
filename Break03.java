public class Break03
{ // about branching-operation; break
  public static void main (String[] args)
  {
    int[] Object1 = new int[] {9, 8, 7, 6, 5};
    int counter, count = Object1.length;
    for (counter = 0; counter < count; counter++)
      if (counter == 2)
        break;
      else
        System.out.print (Object1[counter]);
    System.out.println (counter);
  }
}