import java.util.*;

public class Generic02
{
  public static void main (String[] args)
  { 
    ArrayList<Object> CollectionObject1 = new ArrayList<Object>();
    CollectionObject1.add (true);
    CollectionObject1.add ('a');
    CollectionObject1.add (0.25);
    CollectionObject1.add (-123);
    System.out.println ("Item at subscript 2: " + CollectionObject1.get(2));
    for (Object an_object : CollectionObject1)
    System.out.println (an_object);
  }
}