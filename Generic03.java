public class Generic03
{ 
  static void Insert (Object[] Operand1, Object operand2, int subscript)
  {
    Operand1[subscript] = operand2;
  }
  
  public static void main (String[] args)
  {
    Boolean object1 = true;
    Byte object2 = 1;
    Character object3 = 'c';
    Double object4 = 1.5;
    Float object5 = -0.5f;
    Integer object6 = 6;
    Long object7 = 7L;
    Short object8 = (short)8; 
    Object[] ArrayObject1 = new Object[8];
    Insert (ArrayObject1, object1, 0);
    Insert (ArrayObject1, object2, 1);
    Insert (ArrayObject1, object3, 2);
    Insert (ArrayObject1, object4, 3);
    Insert (ArrayObject1, object5, 4);
    Insert (ArrayObject1, object6, 5);
    Insert (ArrayObject1, object7, 6);
    Insert (ArrayObject1, object8, 7);
    for (int counter = 0; counter < ArrayObject1.length; counter++)
      System.out.println (ArrayObject1[counter]);
  }
}