public class Compare04
{ // assigning collection-value without dynamic allocation
  public static void main (String[] args)
  { // Memory is not dynamically allocated for integer-collection value
    String Object1 = "A string value";
    String Object2 = Object1; // for Object2
    String Object3 = "A string value";
    if (Object1 == Object2)
      System.out.println ("Equal collection-value");
    else
      System.out.println ("Distinct collection-value");
    if (Object1 == Object3)
      System.out.println ("Equal collection-value");
    else
      System.out.println ("Distinct collection-value");
  }
}