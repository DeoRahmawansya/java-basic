public class Compare09
{ // assigning record-value without dynamic allocation

  static boolean equals (int[] A, int[] B)
  {
    if (A.length != B.length)
      return (false);
    short counter, count = (short) A.length;
    for (counter = 0; counter < count; counter++)
      if (A[counter] != B[counter])
        return (false);
    return (true);
  }
  
  public static void main (String[] args)
  { // Memory is not dynamically allocated for record-value
    int[] Object1 = new int[] {1, 2};
    int[] Object2 = new int[] {1, 2};
    Assign03 Object3 = new Assign03('a'); 
    Assign03 Object4 = new Assign03('a');
    System.out.println (equals(Object1, Object2));
    if (Object3.column1 == Object4.column1)
      System.out.println ("Equal record-value");
    else
      System.out.println ("Distinct record-value");
  }
}