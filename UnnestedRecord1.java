class UnnestedRecord1
{
  public char column2;
  public NestedRecord1 Column3;

  UnnestedRecord1 (boolean operand1, char operand2)
  {
    this.Column3 = new NestedRecord1 (operand1);
    this.column2 = operand2;
  }

  public static void main (String[] args)
  {
    UnnestedRecord1 RecordObject2 = new UnnestedRecord1 (true, 'b');
    NestedRecord1 RecordObject3 = RecordObject2.Column3;
    System.out.println (RecordObject2.Column3.column1);
  }
}