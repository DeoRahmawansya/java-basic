class Module19
{
  /* dynamic */ int column1;
  
  Module19 (int operand1)
  { 
    this.column1 = operand1;
  }

  void operation5()
  { // imagine the non OOP approach
    System.out.println (this.column1);  
  } // we prefer the explicit this

  public static void main (String[] args)
  { 
    Module19 RecordObject1 = new Module19 (1);
    Module19 RecordObject2 = new Module19 (3);   
    RecordObject1.operation5(); // operation5 (RecordObject1)
    RecordObject2.operation5(); // operation5 (RecordObject2)
  } // explain the operation-calls above
}   // we do not elaborate operation println