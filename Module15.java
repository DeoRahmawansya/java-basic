class Module15
{
  /* dynamic */ int column1;
  
  Module15 (int operand1)
  { 
    this.column1 = operand1;
  }

  char operation3()
  {
    char that = 'a';
    return (that);  
  }

  public static void main (String[] args)
  { // you canNOT statically allocate ANY record-object
    Module15 RecordObject1 = new Module15 (1);
    operation3();
  } // you can statically allocate any basic-object
}