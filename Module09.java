class Module09
{
  Module09 Module09()
  { // see note in Module09
    Module09 that;
    return (that);
  } // experiment with user-defined symbol

  char operation3()
  {
    char that = 'a';
    return (that);  
  }

  public static void main (String[] args)
  {
    Module09 RecordObject1 = new Module09();
  } 
}   