class UnnestedRecord2
{
  public char column2;
  public NestedRecord2 Column3;

  UnnestedRecord2 (boolean operand1, char operand2)
  {
    this.Column3 = new NestedRecord2 (operand1);
    this.column2 = operand2;
  }

  public static void main (String[] args)
  {
    UnnestedRecord2 RecordObject2 = new UnnestedRecord2 (true, 'b');
    NestedRecord2 RecordObject3 = RecordObject2.Column3;
    System.out.println (RecordObject2.Column3.get());
  }
}