class Module14
{
  /* dynamic */ int column1;
  
  Module14 (int operand1)
  { 
    this.column1 = operand1;
  }

  public static void main (String[] args)
  { // you must dynamically allocate ANY record-object
    Module14 RecordObject1 = new Module14 (1);
    System.out.println (RecordObject1.column1);
  } // then you can operate the columns
}