class X4
{ // required: no arguments
  boolean column1;

  X4 X4 ()
  {
    this.column1 = false;
    return (this);
  }

  X4 X4 (boolean operand)
  {
    this.column1 = operand;
    return (this);
  } // remove return (this);

  public static void main (String[] args)
  {
    X4 RecordObject1 = new X4 ();
    System.out.println (RecordObject1.column1);
    RecordObject1.X4 (true);
    System.out.println (RecordObject1.column1);
  }
}