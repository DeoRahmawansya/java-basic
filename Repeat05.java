public class Repeat05
{ // about repetition-operation; for
  public static void main (String[] args)
  {
    int[] Object1 = new int[] {9, 8, 7, 6, 5};
    int counter = 0, count = Object1.length;
    while (counter < count)
    {
      System.out.print (Object1[counter]);
      counter = counter + 1;
    }
    System.out.println ();
  }
}