public class Label03 
{
  public static void main (String[] args) 
  {
    short i, j;
  level2:
    for (i = 1; i < 5; i++)
    { 
      System.out.println ("i == " + i);
    level1:
      for (j = 1; j < 5; j++)
      {
        if (i > j) 
          continue level1;
        else if (i == 3 && j == 3)
          continue level2;
        System.out.println (" j == " + j);
      }
    }
  } 
}