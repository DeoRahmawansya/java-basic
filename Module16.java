class Module16
{
  /* dynamic */ int column1;
  
  Module16 (int operand1)
  { 
    this.column1 = operand1;
  }

  char operation3()
  {
    char that = 'a';
    return (that);  
  }

  public static void main (String[] args)
  { // you canNOT call operation3()
    Module16 RecordObject1 = new Module16 (1);
    RecordObject1.operation3();
  } // you must call <a-record-object>.operation3()
}