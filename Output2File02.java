import java.io.FileOutputStream;

public class Output2File02 
{
  public static void main (String[] args)
  {
    try
    {
      FileOutputStream OutputFile_Handle = 
        new FileOutputStream("D:\\output_test2.text");
      String Collection_Object1 = "Hello worlding.";    
      // converting char-collection value to byte-collection value
      byte[] Collection_Object2 = Collection_Object1.getBytes();    
      OutputFile_Handle.write (Collection_Object2);    
      OutputFile_Handle.close (); // close the file through handle
      System.out.println ("success..."); // output to screen
    }
    catch (Exception e)
    { System.out.println(e); }
  }
}