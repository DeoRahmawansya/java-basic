class Module18
{
  /* dynamic */ int column1;
  
  Module18 (int operand1)
  { 
    this.column1 = operand1;
  }

  int operation3()
  { // simplifying this.column1 into column1
    return (column1);  
  } // may not mean simplifying your thought

  public static void main (String[] args)
  { 
    Module18 RecordObject1 = new Module18 (1);
    RecordObject1.operation3();
  } 
}