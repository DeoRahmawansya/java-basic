class Module08
{
  Module08 Module08()
  { // replacing system-defined symbol this 
    Module08 this;
    return (this);
  } // with user-defined symbol wiil raise error

  char operation3()
  {
    char that = 'a';
    return (that);  
  }

  public static void main (String[] args)
  {
    Module08 RecordObject1 = new Module08();
  } 
}   