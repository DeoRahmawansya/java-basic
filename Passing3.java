class Passing3
{
  public static void swap (NotEmpty operand1, NotEmpty operand2)
  {
    NotEmpty temp = new NotEmpty(' ');
    temp = operand1;
    operand1 = operand2;
    operand2 = temp;
  }
  
  public static void main (String[] args)
  {
    NotEmpty Object1 = new NotEmpty('a');
    NotEmpty Object2 = new NotEmpty('b');
    System.out.println 
      ("Values before swapping: " + Object1.column1 + ", " + Object2.column1);
    swap (Object1, Object2);
    System.out.println 
      ("Values after swapping: " + Object1.column1 + ", " + Object2.column1); 
  }
}