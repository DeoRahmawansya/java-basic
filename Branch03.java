public class Branch03
{ // need not be rewritten
  public static void main (String[] args)
  { // equals Branch02
    int object1 = 2;
    switch (object1)
    {
      case 1 : System.out.println ("Value is 1"); break;
      case 2 : System.out.println ("Value is 2"); break;
      default: System.out.println ("Value is neither 1 nor 2");
    }
  }
}