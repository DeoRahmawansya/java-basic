class Module17
{
  /* dynamic */ int column1;
  
  Module17 (int operand1)
  { 
    this.column1 = operand1;
  }

  int operation3()
  { // you neither pass record-object this
    return (this.column1);  
  } // nor define/declare record-object this

  public static void main (String[] args)
  { 
    Module17 RecordObject1 = new Module17 (1);
    RecordObject1.operation3();
  } 
}