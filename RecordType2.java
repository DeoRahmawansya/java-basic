public class RecordType2 
{
    public char column1;

    public RecordType2 (char operand1)
    {
      this.column1 = operand1;
    }

    public static void main (String[] args) 
    {
     RecordType2[] CollectionObject1 = 
      {
        new RecordType2 ('a'),
        new RecordType2 ('b'),
        new RecordType2 ('c')
      }
    ;

     System.out.println (CollectionObject1[0].column1);
     System.out.println (CollectionObject1[1].column1);
     System.out.println (CollectionObject1[2].column1);
    }   
}