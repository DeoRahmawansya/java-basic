public class Generic01
{
  public static void main (String[] args)
  {
    Boolean object1 = true;
    Byte object2 = 1;
    Character object3 = 'c';
    Double object4 = 1.5;
    Float object5 = -0.5f;
    Integer object6 = 6;
    Long object7 = 7L;
    Short object8 = (short)8;
    Object[] ArrayObject1 = new Object[8];
    ArrayObject1[0] = object1;
    ArrayObject1[1] = object2;
    ArrayObject1[2] = object3;
    ArrayObject1[3] = object4;
    ArrayObject1[4] = object5;
    ArrayObject1[5] = object6;
    ArrayObject1[6] = object7;
    ArrayObject1[7] = object8;
  }
}