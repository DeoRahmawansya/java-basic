public class Passing5
{
  int column1;
  
  Passing5 (int initial_value)
  {
    column1 = initial_value;
  }
  
  static void operation1 (Passing5 an_operand)
  {
    an_operand.column1 += 1;
  }
  
  public static void main (String[] args)
  {
    Passing5 Object1 = new Passing5 (1);
    System.out.println (Object1.column1);    
    operation1 (Object1); // explicit operand
    System.out.println (Object1.column1); 
  }
}